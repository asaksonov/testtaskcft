//
//  ImageModelProtocol.swift
//  TaskTestCFT
//
//  Created by Alexandr Saxonov on 27.07.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.
//

import UIKit

protocol ImageModelProtocol {
    var image: UIImage { get }
    var isLoad: Bool { get set }
    mutating func setIsLoad()
}
