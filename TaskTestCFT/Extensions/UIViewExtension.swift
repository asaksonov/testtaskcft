//
//  UIViewExtension.swift
//  TaskTestCFT
//
//  Created by Alexandr Saxonov on 27.07.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.
//

import UIKit
extension UIView {
    
    func loadFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
}
