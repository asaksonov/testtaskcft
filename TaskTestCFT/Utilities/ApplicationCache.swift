//
//  ApplicationCache.swift
//  TaskTestCFT
//
//  Created by Alexandr Saxonov on 27.07.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.
//

protocol TokenAccessable {
    func saveToken(_ token: String)
    func resetToken()
    var token: String? { get }
}

protocol Storable: TokenAccessable {}

final class ApplicationCache: Storable {
    
    func saveToken(_ token: String) {
        self.token = token
    }
    
    func resetToken() {
        self.token = nil
    }

    // MARK: - Private Properties
   
    private(set) var token: String?
}
