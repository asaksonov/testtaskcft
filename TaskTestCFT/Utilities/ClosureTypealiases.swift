//
//  ClosureTypealiases.swift
//  TaskTestCFT
//
//  Created by Alexandr Saxonov on 27.07.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.
//

import UIKit

typealias Action = () -> Void

typealias ImageAction = (UIImage?) -> Void

typealias InsertTextBlock = (String) -> Void

typealias IndexBlock = (Int) -> Void
