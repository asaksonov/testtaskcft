//
//  BaseCoordinator.swift
//  TaskTestCFT
//
//  Created by Alexandr Saxonov on 27.07.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.
//

import UIKit

protocol Coordinatable: class {
    var finishFlow: Action? { get set }
    func start()
}

class BaseCoordinator: Coordinatable {

    var childCoordinators: [Coordinatable] = []
    var finishFlow: Action?
    
    func start() {}

    func addDependency(_ coordinator: Coordinatable) {
        for element in childCoordinators where element === coordinator {
             return
        }
        childCoordinators.append(coordinator)
    }

    func removeDependency(_ coordinator: Coordinatable?) {
        guard
            childCoordinators.isEmpty == false,
            let coordinator = coordinator
            else { return }

        for (index, element) in childCoordinators.enumerated() where element === coordinator {
                childCoordinators.remove(at: index)
                break
        }
    }
}
