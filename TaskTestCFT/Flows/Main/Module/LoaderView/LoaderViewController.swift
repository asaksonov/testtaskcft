//
//  LoaderViewController.swift
//  TaskTestCFT
//
//  Created by Mac Mini on 04.08.2019.
//  Copyright © 2019 Mac Mini. All rights reserved.
//

import UIKit

final class LoaderViewController: BaseViewController, LoaderViewOutput {
    var onStertButton: Action?
    
    @IBOutlet weak var startButton: UIButton! {
        didSet {
            startButton.setTitle("Alert.Button.Go".localized, for: .normal)
            startButton = startButton.addDesignStyle()
        }
    }
    @IBAction func onStartButton(_ sender: Any) {
        self.onStertButton?()
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
