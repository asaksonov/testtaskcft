//
//  ModuleFactoryImp.swift
//  TaskTestCFT
//
//  Created by Alexandr Saxonov on 27.07.2019.
//  Copyright © 2019 Alexandr Saxonov. All rights reserved.
//

final class ModuleFactoryImp: ModuleFactoryList {
    
    // MARK: - LoaderModuleFactory
    
    func makeMainModule() -> MainViewInput & MainViewOutput {
        return MainViewController.controllerFromStoryboard(.main)
    }
    
    func makeLoaderModule() -> LoaderViewOutput {
        return LoaderViewController.controllerFromStoryboard(.main)
    }
}
